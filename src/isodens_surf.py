#                                             #
#     AUTHOR: CHANG LIU, 2019-2020            #   
#                                             #
#  ISO-DENSITY SURFACE MODULE FOR DESCRIPTORS #
#                                             #
#          REQUIRES ASE TO USE                #
###############################################

import numpy as np
import scipy.ndimage as ndimage
from ase.data.vdw_alvarez import vdw_radii
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms

def nonzero_mean(matrix, axis, debug = False):
    if debug: #thickness in voxel
        return (matrix!=0).sum(axis)
    else: #nonzero mean
        return np.divide(matrix.sum(axis),(matrix!=0).sum(axis),out=np.zeros_like(matrix.sum(axis)),where=(matrix!=0).sum(axis)!=0)

def plot_axis_mean(name_vr, name_Ir, name_Er, save_name, axis = 'z',nonzero = True):
    from ase.io import read
    import matplotlib.pyplot as plt

    mean_ind = {'x':(1,2),'y':(0,2),'z':(0,1)}
    axis_ind = 3-sum(mean_ind[axis])
    colors = ['k','r','b']
    data_list=[]
    for name in [name_vr, name_Ir, name_Er]:
        dct = read(name,format='cube', read_data=True,full_output=True)
        axis_len = np.linalg.norm(dct['atoms'].get_cell()[axis_ind])
        nd = dct['data'].shape[axis_ind]
        if nonzero:
            data_list.append([np.linspace(0,axis_len,nd),
                              np.ma.masked_equal((dct['data']),0).mean(axis=mean_ind[axis])])
        else:
            data_list.append([np.linspace(0,axis_len,nd),
                              (dct['data']).mean(axis=mean_ind[axis])])

    plt.cla()
    plt.clf()
    fig, (ax1, ax3)= plt.subplots(2,sharex=True)

    ax1.set_title('Axis Average')
    ax1.set_ylabel('V(r)[eV]',color=colors[0])
    ax1.plot(data_list[0][0],data_list[0][1],color=colors[0])

    ax2 = ax1.twinx()
    ax2.set_ylabel('I(r)[eV]',color=colors[1])
    ax2.plot(data_list[1][0],data_list[1][1],color=colors[1])
    ax2.tick_params(axis='y', labelcolor=colors[1])

    ax3.set_xlabel('%s-axis[$\mathrm{\AA}$]'%axis)
    ax3.set_ylabel('V(r)[eV]',color=colors[0])
    ax3.plot(data_list[0][0],data_list[0][1],color=colors[0])

    ax4 = ax3.twinx()
    ax4.set_ylabel('E(r)[eV]',color=colors[2])
    ax4.plot(data_list[2][0],data_list[2][1],color=colors[2])
    ax4.tick_params(axis='y', labelcolor=colors[2])

    fig.tight_layout()
    plt.savefig(save_name,dpi=160)
    plt.close()

'''
def unpad(x, pad_width):
    slices = []
    for c in pad_width:
        e = None if c[1] == 0 else -c[1]
        slices.append(slice(c[0], e))
    return x[tuple(slices)]
'''

def Isodens_surf(atoms, dens, desc, pbc = (0,0,0), isovalue=1e-3, tol = None, scaled = False, rmax = 10.,vdw_r = True, local_extreme = True, local_order = None, local_size = None, debug_mode=False):
    #returns: isosurface of a descriptor, max and min descriptor values for each atom and the coordinates
    #dens: all electron density
    #desc: descriptor, should have the same grid as dens
    #pbc: periodic boundary condition in 3 dimensions
    #isovalue: scaled central value for an isosurface
    #tol: scaled tolerance, 10% of isovalue by default
    #scaled: whether the isovalue is scaled by cell volume times 10, for CHGCAR format use only
    #rmax: maximum distance between voxel and atom for atomic ownership list, unit: A
    #vdw_r: consider vdw radii weighting or not
    #local_extreme: if the extreme points are local
    #local_order: optional for local_extreme, the connectivity of extreme filter, non-cubic, default:2
    #local_size: optional for local_extreme, size of the extreme filter, cubic, ignored if local_order is given
    
    cell = atoms.get_cell()
    anrlst = atoms.get_atomic_numbers()
    if vdw_r:
        vrlst = vdw_radii[anrlst]
    else:
        vrlst = np.ones_like(anrlst)
    if scaled:
        volume = atoms.get_volume()
        scaling_factor = 10*volume
    else:
        scaling_factor = 1.0
    median = isovalue * scaling_factor
    if tol == None:
        tol = np.absolute(0.1*isovalue)
    tol *= scaling_factor
    llim = median - tol
    ulim = median + tol
    mask = (dens > llim) & (dens < ulim) #identify isosurface region
    isosurf = np.where(mask,desc,np.full_like(dens,0.)) #iso-surface, result 1
    isosurf_0 = np.where(mask,desc,np.full_like(dens,np.amin(desc))) #iso-surface 0
    isosurf_1 = np.where(mask,desc,np.full_like(dens,np.amax(desc))) #iso-surface 1
    
    pos_list = atoms.get_positions()
    nx, ny, nz = dens.shape
    x, y, z = np.linspace(0,1,nx), np.linspace(0,1,ny), np.linspace(0,1,nz)
    scaled_pos = np.array(np.meshgrid(x,y,z,indexing = 'ij',sparse=False)).reshape(3,1,nx,ny,nz)
    cell_den = np.array(cell).reshape(3,3,1,1,1)
    coord = np.sum(cell_den * scaled_pos,axis = 0)#coordinate matrix in Angstrom, shape: (3,nx,ny,nz)

    #local extreme finding
    if local_extreme:
        if local_size == None:
            if local_order == None:
                local_order = 2

        elif local_order != None:
            local_size = None

        if local_order != None:
            footprint = ndimage.morphology.generate_binary_structure(len(dens.shape),1)
            if local_order > 1:
                exp_order = local_order - 1#expansion order if local order greater than 1
                footprint = np.pad(footprint,((exp_order,exp_order),(exp_order,exp_order),(exp_order,exp_order)),'constant')
                footprint = ndimage.binary_dilation(footprint,iterations=exp_order)
                
        else:
            footprint = None

        mode_dct = {1:'wrap',0:'nearest'}
        #local extreme mask for isosurf
        local_min = (ndimage.minimum_filter(isosurf_1, size = local_size, footprint=footprint)==isosurf_1, mode = [mode_dct[i] for i in pbc])
        local_max = (ndimage.maximum_filter(isosurf_0, size = local_size, footprint=footprint)==isosurf_0, mode = [mode_dct[i] for i in pbc])
        local_min, local_max = local_min & mask, local_max & mask
            
    #to which atom does a voxel "belong"?
    pbc_dct = {0:[0],1:[-1,0,1]}#for pbc correction    
    atv_iter = np.full_like(dens,rmax)#distance to nearest atom for voxels
    atv_imat = np.full_like(dens,np.NaN)#index of nearest atom for voxels
    for i,pos in enumerate(pos_list):
        for j in pbc_dct[pbc[0]]:
            for k in pbc_dct[pbc[1]]:
                for l in pbc_dct[pbc[2]]:
                    pbc_disp = j*cell[0]+k*cell[1]+l*cell[2]
                    atv_mat = np.linalg.norm(coord-(pos+pbc_disp).reshape(3,1,1,1),axis=0)/vrlst[i]#atom-voxel distance matrix
                    mask_atv = (atv_mat * vrlst[i] < rmax) & mask
                    if mask_atv.any():
                        atv_imat = np.where((atv_mat<atv_iter)&mask_atv,np.full_like(dens,i),atv_imat)
                        atv_iter = np.where((atv_mat<atv_iter)&mask_atv,atv_mat,atv_iter)

    #looping over all atoms for finding max/min voxel
    min_list = []
    max_list = []
    for i,pos in enumerate(pos_list):
        mask_gta = (atv_imat == i)
        if mask_gta.any():
            gta_dens = np.where(mask_gta,desc,np.NaN)
            if debug_mode:
                from ase.io import write
                write('isotst_%d.cube'%i,atoms,data=np.where(mask_gta,np.where(mask,desc,np.full_like(dens,0.)),0.0))
            if local_extreme:
                local_min_loc = np.where(local_min & mask_gta)
                local_max_loc = np.where(local_max & mask_gta)
                local_min_xyz = [np.tile(np.arange(3),local_min_loc[0].shape[0])]+[np.repeat(locmin,3) for locmin in local_min_loc]
                local_max_xyz = [np.tile(np.arange(3),local_max_loc[0].shape[0])]+[np.repeat(locmax,3) for locmax in local_max_loc]
                min_list.append([coord[tuple(local_min_xyz)].reshape(local_min_loc[0].shape[0],3),desc[tuple(local_min_loc)]])
                max_list.append([coord[tuple(local_max_xyz)].reshape(local_max_loc[0].shape[0],3),desc[tuple(local_max_loc)]])
            else:
                #Is reduced/fractional coord necessary?
                #voxel with min descriptor value, returns real coordinates and value itself
                i_min = np.unravel_index(np.nanargmin(gta_dens),gta_dens.shape)
                min_list.append([coord[:,i_min[0],i_min[1],i_min[2]],desc[i_min]]) #result 2
                #voxel with max descriptor value, returns real coordinates and value itself
                i_max = np.unravel_index(np.nanargmax(gta_dens),gta_dens.shape)
                max_list.append([coord[:,i_max[0],i_max[1],i_max[2]],desc[i_max]]) #result 3
        else:
            if local_extreme:
                min_list.append([[np.array([np.NaN]*3)],[np.NaN]]) #result 2
                max_list.append([[np.array([np.NaN]*3)],[np.NaN]]) #result 3
            else:
                min_list.append([np.array([np.NaN]*3),np.NaN]) #result 2
                max_list.append([np.array([np.NaN]*3),np.NaN]) #result 3

    return isosurf, min_list, max_list

def cf_plot(uxy, atoms,fname, ax_unit = '$\mathrm{\AA}$', cb_unit = 'eV', fmt = '% .3f', vmin = None, vmax = None, title = None,reverse = False, scatter = False, lmin = None, lmax = None,uxy_debug = None):
    #plotting the contourf map
    if reverse:
        cname = 'gist_rainbow'
        cmin,cmax = 0.0, 0.8
        c1, c2 = 0.27, 0.53
        scolor = ['r','b']
    else:
        cname = 'gist_rainbow_r'
        cmin,cmax = 0.2, 1.0
        c1, c2 = 0.47, 0.73
        scolor = ['b','r']
    if uxy_debug is not None:
        uxy = np.where(uxy_debug == 0, np.nan, uxy)
    cell = atoms.get_cell()
    cmap_0 = mpl.cm.get_cmap(cname,256)
    newcolors = np.array(list(cmap_0(np.linspace(cmin,c1, 170))) + list(cmap_0(np.linspace(c1,c2,230))) + list(cmap_0(np.linspace(c2,cmax,170))))
    cmap = mpl.colors.ListedColormap(newcolors)
    fig, ax = plt.subplots()
    ax.set_xlabel(ax_unit)
    ax.set_ylabel(ax_unit)
    skx, sky = np.arctan(cell[0][1]/cell[0][0]), np.arctan(cell[1][0]/cell[1][1])
    im = ax.imshow(uxy,origin = 'lower',extent=[0,cell[0][0],0,cell[1][1]],vmin = vmin, vmax = vmax, interpolation = 'none',cmap = cmap)
    transform = mtransforms.Affine2D().skew_deg(sky/np.pi*180.,skx/np.pi*180.)
    trans_data = transform + ax.transData
    im.set_transform(trans_data)
    x1, x2, y1, y2 = im.get_extent()
    ax.plot([x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1], "k-", transform=trans_data)
    cbr = plt.colorbar(im,ax = ax,format=fmt)
    cbr.ax.set_title(cb_unit)
    
    if scatter:
        if lmin != None:
            lmin = np.array(lmin)
            ax.scatter(lmin[:,0],lmin[:,1],color = scolor[0], edgecolor = 'k',marker = 'v',label = 'local min')
        if lmax != None:
            lmax = np.array(lmax)
            ax.scatter(lmax[:,0],lmax[:,1],color = scolor[1], edgecolor = 'k',marker = '^',label = 'local max')
        ax.legend(bbox_to_anchor=(0.0,1.0),fontsize='xx-small')

    fig.suptitle(title)
    fig.savefig('%s'%fname,dpi = 160)
    plt.close(fig)

def dens_slicer(atoms, dens, isovalue = 1e-3, tol_0 = 1e-4, part = 'upper', debug = True):
    #debug: True - thickness, False - density value
    llim = isovalue - tol_0
    ulim = isovalue + tol_0
    mask = (dens > llim) & (dens < ulim) #identify isosurface region
    isosurf = np.where(mask,dens,np.full_like(dens,0.)) 
    z_len = isosurf.shape[2]
    uz_len = int(np.ceil(z_len/2.))
    if part == 'upper':
        desc_u = isosurf[:,:,uz_len:]
    elif part == 'lower':
        desc_u = isosurf[:,:,0:uz_len]
    elif part == 'all':
        desc_u = isosurf
    else:
        raise ValueError("Valid keywords:'upper', 'lower' and 'all'.")
    mxy = nonzero_mean(desc_u,2,debug = debug).T
    dv_min = np.amin(mxy)
    dv_avg = np.average(mxy)
    return mxy, dv_min, dv_avg

def isodens_iter(atoms,dens, dmax, isovalue = 1e-3, tol_0 = 1e-4, part = 'upper', step = 1e-5, maxstep = 10, logfile = 'isoiter_log.txt'):
    #dmax: max average thickness in voxel
    #tol_0: initial tolerance
    with open(logfile,'a') as f:
        f.write("""\
=========================================================================
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Please make sure the model is topologically sound, e.g. periodic surface.
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
=========================================================================

""")

    tol = tol_0
    i = 0
    with open(logfile,'a') as f:
        f.write("""\
intended upper limit of thickness: % .2f
initial tolerance(a.u.): % .2e
"""%(dmax,tol_0))
        f.write(" iter      tol           min.d     avg.d        \n")
    while i < maxstep:
        mxy, dv_min, dv_avg = dens_slicer(atoms,dens, isovalue, tol_0 = tol)
        with open(logfile,'a') as f:
            f.write("%5d     % 1.4e   % .2f     % .2f\n"%(i,tol,dv_min,dv_avg))
        if dv_min < 1: 
            if dv_avg < dmax:
                tol += step
            elif dv_avg >= dmax:
                with open(logfile,'a') as f:
                    f.write('dmax is too low!')
                raise RuntimeError("dmax is too low!")

        elif dv_min >= 1:
            if dv_avg <= dmax:
                with open(logfile,'a') as f:
                    f.write("""
Final results:% 1.4e   % .2f     % .2f

-------------------------------------------------------------------------
"""%(tol,dv_min,dv_avg))
                return mxy, tol, dv_min, dv_avg
            elif dv_avg > dmax:
                tol -= step
        i += 1
    with open(logfile,'a') as f:
        f.write("Please increase 'maxstep' and decrease 'step'!")
    raise ValueError("Please increase 'maxstep' and decrease 'step'!")

def cube_cutter(atoms,dens,a_1,a_2,b_1,b_2,c_1,c_2):
    #function for cutting cube files:
    #a_1, a_2: beginning and ending points along a, in Angstrom
    #b_1, b_2: beginning and ending points along b, in Angstrom
    #c_1, c_2: beginning and ending points along c, in Angstrom
    cell = atoms.get_cell()
    dshape = dens.shape    
    par = [] #a_1, a_2, b_1, b_2, c_1 and c_2 in voxel unit
    for i,p in enumerate([[a_1,a_2],[b_1,b_2],[c_1,c_2]]):
        vect = []
        for q in p:
            q = np.floor(q/np.linalg.norm(cell[i])*dshape[i])
            vect.append(q)
        par.append(vect)
    par = np.array(par)

    #new cell
    cell_new = np.zeros_like(cell)
    for i in range(3):
        cell_new[i] = cell[i] * (par[i][1]-par[i][0])/dshape[i]
        
    #new positions
    pos = atoms.get_scaled_positions()
    pos -= np.array([par[0][0]/dshape[0],par[1][0]/dshape[1],par[2][0]/dshape[2]]).reshape(1,3)
    atoms.set_scaled_positions(pos)
    
    atoms.set_cell(cell_new)#atoms' changes done

    par = par.astype('int')
    #new dens
    dens_new = unpad(dens,((par[0][0],dshape[0]-par[0][1]),(par[1][0],dshape[1]-par[1][1]),(par[2][0],dshape[2]-par[2][1])))
    return atoms, dens_new

def cube_center_cut(atoms,dens,cell_new):
    #function to cut a cube file with a new cell
    #note that it cuts off six edges symmetrically
    #cell_new: new cell, smaller than the old one
    cell = atoms.get_cell()
    cell_abc = []
    for i, c in enumerate([cell,cell_new]):
        cell_t = []
        for j in range(3):
            cell_t.append(np.linalg.norm(c[j]))
        cell_abc.append(cell_t)
    cell_abc = np.array(cell_abc)
    cell_diff = (cell_abc[0] - cell_abc[1])/2.
    cell_sum = (cell_abc[0] + cell_abc[1])/2.
    atoms_new, dens_new = cube_cutter(atoms,dens,cell_diff[0],cell_sum[0],cell_diff[1],cell_sum[1],cell_diff[2],cell_sum[2])
    return atoms_new, dens_new
