#
# DESCRIPTOR MODULE FOR GPAW, AUTHOR: CHANG LIU
#
# DEVELOPED IN 2019
#                                           
# WORKS FOR SPIN-POLARIZED CALCULATIONS
#
#############################################
from __future__ import print_function
import numpy as np
from gpaw.utilities.ps2ae import PS2AE,Interpolator
from gpaw.grid_descriptor import GridDescriptor
from gpaw.mpi import serial_comm
from gpaw.transformers import Transformer
from ase.units import Bohr, Ha
from ase.parallel import parprint
from copy import deepcopy

def local_electron_affinity_band(kpt_weight,band_eigenvalue,band_density,E_0):
    """Takes, for a given point in space, the necessary parameters to calculate
    a bands contribution to E(r) * rho(r) for an arbitrary kpoint, unscaled!
    kpt_weight = kpoint weight
    band_eigenvalue = KS-eigenvalue for band at given kpoint
    band_density = local electron density for band at kpoint 
    E_0 = correction factor for referens potential (i.e. potential in vacuum-center) 
    """
    lea = kpt_weight * (band_eigenvalue - E_0) * band_density
 
    return lea

class KSDescriptor(PS2AE):
    def __init__(self,calc, ae=False, gridrefinement = 4, **kwargs):
        PS2AE.__init__(self,calc, **kwargs)
        #for electrostatic potential, to bypass a bug in gpaw:
        #https://listserv.fysik.dtu.dk/pipermail/gpaw-users/2019-June/005689.html
        
        wfs = calc.wfs
        gd = wfs.gd
        gd1 = GridDescriptor(gd.N_c, gd.cell_cv, comm=serial_comm)
        #Descriptor for the final grid
        self.gridrefinement = gridrefinement
        self.ae = ae
        gd_rfd = calc.density.finegd.refine()
        gd2 = self.gd = GridDescriptor(gd_rfd.N_c, gd.cell_cv, comm=serial_comm)
        self.interpolator = Interpolator(gd1, gd2, self.calc.wfs.dtype)

        self.v_matrix = deepcopy(self.electrostatic_potential(initialize=True))
        v_matrix = self.v_matrix
        planar = v_matrix.mean(axis=(0,1)) # average over x and y
        self.E_0 = np.max(planar)
        
        nkpts = len(calc.get_ibz_k_points())
        self.nkpts = nkpts
        # find number of occupied orbitals, if no fermi smearing
        self.nocc = []
        if calc.get_spin_polarized() == 0:
            self.nocc.append(wfs.nvalence // 2)
        else:
            for spin in range(2):
                noc = 0.0
                for kpt in range(nkpts):
                    noc += sum(calc.get_occupation_numbers(kpt=kpt, spin=spin))
                noc = int(noc + 0.5)
                parprint('spin:', spin, 'nocc', noc)
                self.nocc.append(noc)
        parprint('number of valence electrons:',wfs.nvalence)

    def electrostatic_potential(self,initialize = False):
        ae = self.ae
        if initialize:
            v_R = self.get_electrostatic_potential(ae=ae)
            return v_R
        return self.v_matrix

    def reference_potential(self):
        #determines average potential in the vacuum center/maximum potential in slab center 
        return self.E_0

    def parchg_density(self,band=0, kpt=0, spin=0,ae = False):
        #charge density for a band
        #spin-kpts, spin always zero!
        pswf = self.get_wave_function(band,kpt,spin,ae = ae)
        return (pswf * np.conjugate(pswf)).real

    def get_electron_density(self):
        ae = self.ae
        if ae:
            return self.calc.get_all_electron_density(gridrefinement=self.gridrefinement)
        return self.pseudo_density()

    def pseudo_density(self,pad = True,broadcast = True):
        gd = self.calc.density.finegd.refine()
        interpolator = Transformer(self.calc.density.finegd, gd, 3)
        nt_sg = gd.empty(self.calc.density.nspins)
        if self.calc.density.nt_sg is None:
            self.calc.density.interpolate_pseudo_density()
        for s in range(self.calc.density.nspins):
            interpolator.apply(self.calc.density.nt_sg[s], nt_sg[s])

        if self.calc.density.nspins == 1:
            nt_G = nt_sg[0]
        else:
            nt_G = nt_sg.sum(axis=0)

        nt_G = gd.collect(nt_G, broadcast=broadcast)

        if nt_G is None:
            return None

        if pad:
            nt_G = gd.zero_pad(nt_G)

        return nt_G / Bohr**3

    def density_descriptor(self,mode,e_r = None):
        ae = self.ae
        #use: mode = 'AIP' or 'MEA'
        #e_r: the eigen energy range of orbitals included
        implemented_modes = ['AIP','MEA']
        if mode not in implemented_modes:
            raise RuntimeError("Use either 'AIP' or 'MEA'!")
        parprint("Mode: %s"%mode)
  
        E_0 = self.reference_potential()
        dens = self.get_electron_density()
        desc_mat = np.zeros_like(dens)
        nspins = self.calc.get_spin_polarized() + 1
        weights = self.calc.get_k_point_weights()
        for spin in range(nspins):
            for kpt in range(self.nkpts):
                eigen_energies = self.calc.get_eigenvalues(kpt = kpt,spin = spin)
                n_occ = self.nocc[spin]
                e_fermi = (eigen_energies[n_occ-1] + eigen_energies[n_occ]) / 2 
                #I(r)/AIP, for occupied states
                if mode == 'AIP':
                    n_end = n_occ
                    if e_r == None:
                        n_start = 0
                        n = n_occ
                    else:
                        mask = (eigen_energies >= e_fermi - e_r)
                        eps_v = eigen_energies[mask]
                        n_start = len(eigen_energies) - len(eps_v)
                        n = len(eps_v)
                #E(r)/MEA, for unoccupied states
                if mode == 'MEA':
                    v_ref = self.reference_potential()
                    n_start = n_occ
                    mask = (eigen_energies < v_ref)
                    if e_r != None:
                        mask = mask & (eigen_energies <= e_fermi + e_r)
                    eps_v = eigen_energies[mask]
                    n_end = len(eps_v)
                    n = n_end - n_start
                    if n <= 0:
                        raise RuntimeError("Reference potential below Fermi level!")
                parprint("Start: %d; End: %d; Number: %d"%(n_start,n_end-1,n))
                #list of band density
                for band in range(n_start,n_end):
                    parchg_band = self.parchg_density(band=band,kpt = kpt,spin=spin,ae=ae)
                    desc_band = local_electron_affinity_band(weights[kpt],eigen_energies[band],parchg_band,E_0)
                    desc_mat += desc_band
                parprint('kpt:%d done'%kpt)
        desc_mat = np.divide(desc_mat,dens,out=np.zeros_like(desc_mat),where=dens!=0)
        if mode == 'AIP':
            desc_mat = -desc_mat
        #counting both spin systems also for spin-paired cases!
        if nspins == 1:
            desc_mat *= 2
        return desc_mat
